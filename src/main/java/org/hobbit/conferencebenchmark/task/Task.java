package org.hobbit.conferencebenchmark.task;

import java.io.Serializable;

/**
 * Re-used from the LargeBio track
 *
 * @author ernesto
 * @author ondrej
 * 
 * Modifications by ondrej
 *
 */

public class Task implements Serializable {
	private String taskId;
	//private String source_name;
	//private String target_name;
	private byte[] source_target;
	private byte[] expectedAnswers;
	//13-02-18
	private String taskQueueName; //To be use as queue name as well
	
	//To control what to match in the task
	private boolean isMatchingClassesRequired = true;
	private boolean isMatchingObjectPropertiesRequired = true;
	private boolean isMatchingDataPropertiesRequired = true;
	private boolean isMatchingInstancesRequired = false;
	
	
	
	public Task(String id) {
		this.taskId = id;
	}
	
	public void setId(String id) {
		this.taskId = id;
	}
	
	public String getTaskId() {
		return this.taskId;
	}
	
	/*
	public void setSourceName(String name) {
		this.source_name = name;
	}
	
	public String getSourceName() {
		return this.source_name;
	}
	
	public void setTargetName(String name) {
		this.target_name = name;
	}
	
	public String getTargetName() {
		return this.target_name;
	}
	*/
	
	public void setSourceTarget(byte[] target) {
		this.source_target =  target;
	}
	
	public byte[] getSourceTarget() {
		return this.source_target;
	}
	    
	public void setExpectedAnswers(byte[] res) {
		this.expectedAnswers =  res;
	}
	
	public byte[] getExpectedAnswers() {
		return this.expectedAnswers;
	}

	/**
	 * @return the isMatchingClassesRequired
	 */
	public boolean isMatchingClassesRequired() {
		return isMatchingClassesRequired;
	}

	/**
	 * @param isMatchingClassesRequired the isMatchingClassesRequired to set
	 */
	public void setMatchingClassesRequired(boolean isMatchingClassesRequired) {
		this.isMatchingClassesRequired = isMatchingClassesRequired;
	}

	/**
	 * @return the isMatchingPropertiesRequired
	 */
	public boolean isMatchingDataPropertiesRequired() {
		return isMatchingDataPropertiesRequired;
	}

	/**
	 * @param isMatchingPropertiesRequired the isMatchingPropertiesRequired to set
	 */
	public void setMatchingDataPropertiesRequired(boolean isMatchingDataPropertiesRequired) {
		this.isMatchingDataPropertiesRequired = isMatchingDataPropertiesRequired;
	}

	/**
	 * @return the isMatchingInstancesRequired
	 */
	public boolean isMatchingInstancesRequired() {
		return isMatchingInstancesRequired;
	}

	/**
	 * @param isMatchingInstancesRequired the isMatchingInstancesRequired to set
	 */
	public void setMatchingInstancesRequired(boolean isMatchingInstancesRequired) {
		this.isMatchingInstancesRequired = isMatchingInstancesRequired;
	}

	/**
	 * @return the isMatchingObjectPropertiesRequired
	 */
	public boolean isMatchingObjectPropertiesRequired() {
		return isMatchingObjectPropertiesRequired;
	}

	/**
	 * @param isMatchingObjectPropertiesRequired the isMatchingObjectPropertiesRequired to set
	 */
	public void setMatchingObjectPropertiesRequired(boolean isMatchingObjectPropertiesRequired) {
		this.isMatchingObjectPropertiesRequired = isMatchingObjectPropertiesRequired;
	}

	/**
	 * @return the taskName or queueName to identify the files to send/receive
	 */
	public String getTaskQueueName() {
		return taskQueueName;
	}

	/**
	 * @param taskName the taskName to set
	 */
	public void setTaskQueueName(String taskName) {
		this.taskQueueName = taskName;
	}

}

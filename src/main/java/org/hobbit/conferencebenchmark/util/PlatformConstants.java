package org.hobbit.conferencebenchmark.util;

import java.io.File;

/**
 * Re-used from the LargeBio track
 *
 * @author ernesto
 * @author ondrej
 * 
 * Modifications by ondrej on 12 Jan 2018
 *
 */

public class PlatformConstants {

	 // =============== DATA GENERATOR CONSTANTS ===============
   public static final String TASK = "conference_task";

   // =============== EVALUATION MODULE CONSTANTS ===============

   public static final String EVALUATION_RECALL = "evaluation_recall";
   public static final String EVALUATION_PRECISION = "evaluation_precision";
   public static final String EVALUATION_FMEASURE = "evaluation_fmeasure";
   public static final String EVALUATION_TIME_PERFORMANCE = "evaluation_time_performance";

   
   
   // =============== INTERNAL FORLDERS FOR THE BENCHMARK =============== 
   public static String datasetsPath = System.getProperty("user.dir") + File.separator + "datasets";
   
   
   
   // =============== URIS (used in benchmark.ttl) ===============
   
   public static final String bench_ns = "http://w3id.org/bench#";
   
   //KPIs
   public static final String PRECISION_URI = bench_ns + "precision";
   public static final String RECALL_URI = bench_ns + "recall";
   public static final String FMEASURE_URI = bench_ns + "fmeasure";
   public static final String TIME_URI = bench_ns + "timePerformance";
   
   //Tasks
   public static final String TASK_CMT_CONFERENCE_URI =  bench_ns + "cmt-conference";
   public static final String TASK_CMT_CONFOF_URI =  bench_ns + "cmt-confof"; 
   public static final String TASK_CMT_EKAW_URI =  bench_ns + "cmt-ekaw";
   public static final String TASK_CMT_EDAS_URI =  bench_ns + "cmt-edas";
   public static final String TASK_CMT_IASTED_URI =  bench_ns + "cmt-iasted";
   public static final String TASK_CMT_SIGKDD_URI =  bench_ns + "cmt-sigkdd";
   
   public static final String TASK_CONFERENCE_EKAW_URI =  bench_ns + "conference-ekaw";
   public static final String TASK_CONFERENCE_CONFOF_URI =  bench_ns + "conference-confof";
   public static final String TASK_CONFERENCE_EDAS_URI =  bench_ns + "conference-edas";
   public static final String TASK_CONFERENCE_IASTED_URI =  bench_ns + "conference-iasted";
   public static final String TASK_CONFERENCE_SIGKDD_URI =  bench_ns + "conference-sigkdd";
   
   public static final String TASK_CONFOF_EDAS_URI =  bench_ns + "confof-edas";
   public static final String TASK_CONFOF_EKAW_URI =  bench_ns + "confof-ekaw";
   public static final String TASK_CONFOF_IASTED_URI =  bench_ns + "confof-iasted";
   public static final String TASK_CONFOF_SIGKDD_URI =  bench_ns + "confof-sigkdd";
   
   public static final String TASK_EDAS_EKAW_URI =  bench_ns + "edas-ekaw";
   public static final String TASK_EDAS_IASTED_URI =  bench_ns + "edas-iasted";
   public static final String TASK_EDAS_SIGKDD_URI =  bench_ns + "edas-sigkdd";
   
   public static final String TASK_EKAW_IASTED_URI =  bench_ns + "ekaw-iasted";
   public static final String TASK_EKAW_SIGKDD_URI =  bench_ns + "ekaw-sigkdd";
   
   public static final String TASK_IASTED_SIGKDD_URI =  bench_ns + "iasted-sigkdd";
   
   public static final String TASK_CMT_CONFERENCE =  "cmt-conference";
   public static final String TASK_CMT_CONFOF =  "cmt-confof"; 
   public static final String TASK_CMT_EKAW =  "cmt-ekaw";
   public static final String TASK_CMT_EDAS =  "cmt-edas";
   public static final String TASK_CMT_IASTED =  "cmt-iasted";
   public static final String TASK_CMT_SIGKDD =  "cmt-sigkdd";
   
   public static final String TASK_CONFERENCE_EKAW =  "conference-ekaw";
   public static final String TASK_CONFERENCE_CONFOF =  "conference-confof";
   public static final String TASK_CONFERENCE_EDAS =  "conference-edas";
   public static final String TASK_CONFERENCE_IASTED =  "conference-iasted";
   public static final String TASK_CONFERENCE_SIGKDD =  "conference-sigkdd";
   
   public static final String TASK_CONFOF_EDAS =  "confof-edas";
   public static final String TASK_CONFOF_EKAW =  "confof-ekaw";
   public static final String TASK_CONFOF_IASTED =  "confof-iasted";
   public static final String TASK_CONFOF_SIGKDD =  "confof-sigkdd";
   
   public static final String TASK_EDAS_EKAW =  "edas-ekaw";
   public static final String TASK_EDAS_IASTED =  "edas-iasted";
   public static final String TASK_EDAS_SIGKDD =  "edas-sigkdd";
   
   public static final String TASK_EKAW_IASTED =  "ekaw-iasted";
   public static final String TASK_EKAW_SIGKDD =  "ekaw-sigkdd";
   
   public static final String TASK_IASTED_SIGKDD =  "iasted-sigkdd";
   
   //Parameters
   public static final String TASK_URI = bench_ns + "conferenceTask";

   
   //Serialization Formats
   public static final String RDFXML = "RDFXML";
   public static final String TURTLE = "Turtle";
   
   //
   
}

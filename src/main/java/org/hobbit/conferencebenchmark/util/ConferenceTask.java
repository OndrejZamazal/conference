package org.hobbit.conferencebenchmark.util;

/**
 * Helper class for loading all conference tasks
 *
 * @author ondrej
 * 
 */

public class ConferenceTask {
	
	String ontology1;
	String ontology2;
	String reference_alignment;
	
	public ConferenceTask(String ont1, String ont2, String ra) {
		
		this.ontology1 = ont1;
		this.ontology2 = ont2;
		this.reference_alignment = ra;
		
	}
	
	public void setOntology1(String ont1) {
		this.ontology1 = ont1;
	}
	
	public String getOntology1() {
		return this.ontology1;
	}
	
	public String getOntologyName1() {
		return this.ontology1.substring(0,this.ontology1.lastIndexOf("."));
	}

	public void setOntology2(String ont2) {
		this.ontology2 = ont2;
	}
	
	public String getOntology2() {
		return this.ontology2;
	}
	
	public String getOntologyName2() {
		return this.ontology2.substring(0,this.ontology2.lastIndexOf("."));
	}
	
	public void setReferenceAlignment(String ra) {
		this.reference_alignment = ra;
	}
	
	public String getReferenceAlignment() {
		return this.reference_alignment;
	}
	
}
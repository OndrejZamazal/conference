package org.hobbit.conferencebenchmark;

/**
 * Re-used from the LargeBio track
 *
 * @author ernesto
 * @author ondrej
 * 
 * Modifications by ondrej on 19 Jan 2018
 * towards more tasks in one single TaskGenerator
 * 
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.hobbit.core.Commands;
import org.hobbit.core.components.AbstractTaskGenerator;
import org.hobbit.core.rabbit.RabbitMQUtils;
import org.hobbit.core.rabbit.SimpleFileReceiver;
import org.hobbit.core.rabbit.SimpleFileSender;
import org.hobbit.conferencebenchmark.task.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TaskGenerator extends AbstractTaskGenerator {
	private static final Logger LOGGER = LoggerFactory.getLogger(TaskGenerator.class);
    private SimpleFileReceiver inputFilesReceiver;

    public TaskGenerator() {
        super(1);
    }

	@Override
	public void init() throws Exception {
	
	    LOGGER.info("Initializing Task Generators...");
	    super.init();
	    //TODO here
	    //Sent from Data Generator
	    inputFilesReceiver = SimpleFileReceiver.create(this.incomingDataQueueFactory, "input_files");
	    LOGGER.info("Task Generators initialized successfully.");
	}


	@Override
	protected void generateTask(byte[] data) throws Exception {
		try {
			
			LOGGER.info("Generating tasks. First taking \"input_files\" from DataGenerator...");
			
			//String[] receivedFiles_target = targetReceiver.receiveData("./datasets/");
			//Receive files from DataGenerator: all ontologies regardless whether they are source or target ontologies but in the order
			String[] receivedFiles = inputFilesReceiver.receiveData("./datasets/");
			
			//============== We sent source and target files to System with each task ==============
			
			//Define a queue name to be identifiable by system	        
			String queueName = "";
			
	        //============== We Process information about the task and send it to System Adaptor and Evaluation Storage ==============
	        
			// NOTE my current solution for more tasks is that I send all created tasks at once serialized ArrayList<Task> and now I have to deserialize them and go through them. 
			LOGGER.info("Iterating over tasks and source and target files and sending them to the system (task by task)...");
			//int i=0;
			ArrayList<Task> tasks = (ArrayList<Task>) SerializationUtils.deserialize(data);
			for(Task task : tasks) {
				
				//queue is sorted alphabetically not very handy now
				//File file_source = new File("./datasets/" + receivedFiles[i]);
				//File file_target = new File("./datasets/" + receivedFiles[i+1]);
				//LOGGER.info("Source "+receivedFiles[i]+" and target "+receivedFiles[i+1]+ "files succesfully received from data generator");
				//LOGGER.info("Source "+file_source+" and target "+file_target+ "files succesfully received from data generator");
				//LOGGER.info("Source "+file_source.getName()+" and target "+file_target.getName()+ "files succesfully received from data generator");
				
				// Create an ID for the task
				String taskId = task.getTaskId();
				LOGGER.info("Preparing task "+taskId);
				
				boolean isMatchingClassesRequired = task.isMatchingClassesRequired();
				boolean isMatchingDataPropertiesRequired = task.isMatchingDataPropertiesRequired();
				boolean isMatchingObjectPropertiesRequired = task.isMatchingObjectPropertiesRequired();
				boolean isMatchingInstancesRequired = task.isMatchingInstancesRequired();
				
				byte[] source_target = task.getSourceTarget();
				ByteBuffer taskBuffer = ByteBuffer.wrap(source_target);
				//We read 3 parameters
				String format = RabbitMQUtils.readString(taskBuffer);
				String path_source = RabbitMQUtils.readString(taskBuffer);
				String path_target = RabbitMQUtils.readString(taskBuffer);
				
				path_source = path_source.substring(path_source.lastIndexOf("/")+1);
				path_target = path_target.substring(path_target.lastIndexOf("/")+1);
				
				LOGGER.info("Path to files source "+path_source+" target "+path_target);
				
				// We will send source and target files to System using sender
				
				File file_source = new File("./datasets/" + path_source);
				File file_target = new File("./datasets/" + path_target);
				LOGGER.info("Source "+file_source+" and target "+file_target+ "files succesfully received from data generator");
				LOGGER.info("Source "+file_source.getName()+" and target "+file_target.getName()+ " files succesfully received from data generator");
				
				// create the sender
				//queuName to identify queue according to received ontologies - now it is set up in datagenerator
				//queueName = path_source.substring(0,path_source.lastIndexOf("."))+"-"+path_target.substring(0,path_target.lastIndexOf("."));
				// create the sender with queue name as the task identifier 
				LOGGER.info("queue name is "+task.getTaskQueueName());
				SimpleFileSender sender = SimpleFileSender.create(this.outgoingDataQueuefactory, task.getTaskQueueName());
				
				InputStream is_source = null;
				InputStream is_target = null;
				try {
				    // create input stream, e.g., by opening a file
				    is_source = new FileInputStream(file_source);
				    is_target = new FileInputStream(file_target);
				    // send data
				    sender.streamData(is_source, file_source.getName());
				    sender.streamData(is_target, file_target.getName());
				} catch (Exception e) {
				    // handle exception
				} finally {
				    IOUtils.closeQuietly(is_source);
				    IOUtils.closeQuietly(is_target);
				}
				// close the sender
				IOUtils.closeQuietly(sender);
				LOGGER.info("Source "+file_source.getName()+" and target "+file_target.getName()+" files succesfully send as task_files by sender");
				
				//===================== SYSTEM will receive these 7 parameters
				//This order will be important for the System adapter
				byte[][] taskDataArray = new byte[8][];
				//The first two seems to be "standard" across HOBBIT benchmarks
				//FOR OAEI it is also important to provide both source and target associated to the task
				taskDataArray[0] = RabbitMQUtils.writeString(format);
				//Important to send the name
				taskDataArray[1] = RabbitMQUtils.writeString(file_source.getName());
				LOGGER.info("Source in task "+file_source.getName());
				taskDataArray[2] = RabbitMQUtils.writeString(file_target.getName());
				LOGGER.info("Target in task "+file_target.getName());
				//For OAEI is a desired characteristic
				taskDataArray[3] = RabbitMQUtils.writeString(String.valueOf(isMatchingClassesRequired));
				taskDataArray[4] = RabbitMQUtils.writeString(String.valueOf(isMatchingDataPropertiesRequired));
				taskDataArray[5] = RabbitMQUtils.writeString(String.valueOf(isMatchingObjectPropertiesRequired));
				taskDataArray[6] = RabbitMQUtils.writeString(String.valueOf(isMatchingInstancesRequired));

				//Task name and queueID for system
				//taskDataArray[7] = RabbitMQUtils.writeString(queueName);
				taskDataArray[7] = RabbitMQUtils.writeString(task.getTaskQueueName());
				
				byte[] taskData = RabbitMQUtils.writeByteArrays(taskDataArray);
				
				byte[] expectedAnswerData = task.getExpectedAnswers();
				
				// Send the task to the system (and store the timestamp)
				
				sendTaskToSystemAdapter(taskId, taskData);
				LOGGER.info("Task " + taskId + " sent to System Adapter.");
				
				// Send the expected answer to the evaluation store
				long timestamp = System.currentTimeMillis();
				sendTaskToEvalStorage(taskId, timestamp, expectedAnswerData);
				LOGGER.info("Expected answers of task " + taskId + " sent to Evaluation Storage.");
				
				//i=+2;
			}
			
		} 
		catch (Exception e) {
			LOGGER.error("Exception caught while reading the tasks and their expected answers", e);
		}
		
	}



	@Override
	public void receiveCommand(byte command, byte[] data) {
	   if (Commands.DATA_GENERATION_FINISHED == command) {
		   inputFilesReceiver.terminate();
	   }
	   super.receiveCommand(command, data);
	}

	@Override
	public void close() throws IOException {
	    LOGGER.info("Closign Task Generator...");
	
	    super.close();
	    LOGGER.info("Task Genererator closed successfully.");
	}

}
package org.hobbit.conferencebenchmark;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.jena.rdf.model.NodeIterator;
import org.hobbit.core.Commands;
import org.hobbit.core.components.AbstractBenchmarkController;
import org.hobbit.core.Constants;
import org.hobbit.conferencebenchmark.util.PlatformConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Re-used from the LargeBio track
 *
 * @author ernesto
 * @author ondrej
 * 
 * Modifications by ondrej on 15 Jan 2018
 * trying to use more data generators and task generators at once 
 *
 */

public class BenchmarkController extends AbstractBenchmarkController {

	private static final Logger LOGGER = LoggerFactory.getLogger(BenchmarkController.class);
    private static final String DATA_GENERATOR_CONTAINER_IMAGE = "git.project-hobbit.eu:4567/ondrej.zamazal/conferencedatagenerator1";
    private static final String TASK_GENERATOR_CONTAINER_IMAGE = "git.project-hobbit.eu:4567/ondrej.zamazal/conferencetaskgenerator1";
    private static final String EVALUATION_MODULE_CONTAINER_IMAGE = "git.project-hobbit.eu:4567/ondrej.zamazal/conferenceevaluationmodule1";
	
    
    //KPIs: P, R, F and Time
    private String[] envVariablesEvaluationModule;
	
    //for multiple tasks solution
    //private String[] evalStorageEnvVariables = null;

    @Override
    public void init() throws Exception {
        LOGGER.info("Initializing Benchmark Controller...");
        super.init();
        
        
        //This version benchmark controller has no parameter, i. e. all 21 subtasks (now 21 subtasks of them) will be executed at once. 
        //String conference_task = (String) getProperty(PlatformConstants.TASK_URI, PlatformConstants.TASK_CMT_CONFERENCE_URI);
        
        // data generators environmental values
        /*
        String[] envVariablesDataGenerator = new String[]{
        		PlatformConstants.TASK + "=" + conference_task
            
        	PlatformConstants.TASK + "=" + PlatformConstants.TASK_CMT_CONFERENCE_URI ,
            PlatformConstants.TASK + "=" + PlatformConstants.TASK_CMT_CONFOF_URI ,
            PlatformConstants.TASK + "=" + PlatformConstants.TASK_CMT_EKAW_URI ,
            PlatformConstants.TASK + "=" + PlatformConstants.TASK_CMT_EDAS_URI ,
            PlatformConstants.TASK + "=" + PlatformConstants.TASK_CMT_IASTED_URI ,
            PlatformConstants.TASK + "=" + PlatformConstants.TASK_CMT_SIGKDD_URI ,
            
            PlatformConstants.TASK + "=" + PlatformConstants.TASK_CONFERENCE_CONFOF_URI ,
            PlatformConstants.TASK + "=" + PlatformConstants.TASK_CONFERENCE_EDAS_URI ,
            PlatformConstants.TASK + "=" + PlatformConstants.TASK_CONFERENCE_EKAW_URI ,
            PlatformConstants.TASK + "=" + PlatformConstants.TASK_CONFERENCE_IASTED_URI ,
            PlatformConstants.TASK + "=" + PlatformConstants.TASK_CONFERENCE_SIGKDD_URI ,
            
            PlatformConstants.TASK + "=" + PlatformConstants.TASK_CONFOF_EDAS_URI ,
            PlatformConstants.TASK + "=" + PlatformConstants.TASK_CONFOF_EKAW_URI ,
            PlatformConstants.TASK + "=" + PlatformConstants.TASK_CONFOF_IASTED_URI ,
            PlatformConstants.TASK + "=" + PlatformConstants.TASK_CONFOF_SIGKDD_URI ,
            
            PlatformConstants.TASK + "=" + PlatformConstants.TASK_EDAS_EKAW_URI ,
            PlatformConstants.TASK + "=" + PlatformConstants.TASK_EDAS_IASTED_URI ,
            PlatformConstants.TASK + "=" + PlatformConstants.TASK_EDAS_SIGKDD_URI ,
            
            PlatformConstants.TASK + "=" + PlatformConstants.TASK_EKAW_IASTED_URI ,
            PlatformConstants.TASK + "=" + PlatformConstants.TASK_EKAW_SIGKDD_URI ,
            
            PlatformConstants.TASK + "=" + PlatformConstants.TASK_IASTED_SIGKDD_URI
        };*/

        // get KPIs for evaluation module
        envVariablesEvaluationModule = new String[]{
            PlatformConstants.EVALUATION_RECALL + "=" + PlatformConstants.RECALL_URI,
            PlatformConstants.EVALUATION_PRECISION + "=" + PlatformConstants.PRECISION_URI,
            PlatformConstants.EVALUATION_FMEASURE + "=" + PlatformConstants.FMEASURE_URI,
            PlatformConstants.EVALUATION_TIME_PERFORMANCE + "=" + PlatformConstants.TIME_URI
        };
        
        //for multiple tasks solution
        //evalStorageEnvVariables = ArrayUtils.add(DEFAULT_EVAL_STORAGE_PARAMETERS,
         //       Constants.RABBIT_MQ_HOST_NAME_KEY + "=" + this.rabbitMQHostName);
         //evalStorageEnvVariables = ArrayUtils.add(evalStorageEnvVariables, "ACKNOWLEDGEMENT_FLAG=true");
        
        // Create evaluation storage - for multiple tasks solution
        //createEvaluationStorage(DEFAULT_EVAL_STORAGE_IMAGE, evalStorageEnvVariables);
         
        // Create data generators.
        //int numberOfDataGenerators = 21;
        int numberOfDataGenerators = 1;
        //createDataGenerators(DATA_GENERATOR_CONTAINER_IMAGE, numberOfDataGenerators, envVariablesDataGenerator);
        createDataGenerators(DATA_GENERATOR_CONTAINER_IMAGE, numberOfDataGenerators, new String[]{});
        LOGGER.info("Initilalizing Benchmark Controller...");

               
        //Create task generators
        //int numberOfTaskGenerators = 21;
        int numberOfTaskGenerators = 1;
        createTaskGenerators(TASK_GENERATOR_CONTAINER_IMAGE, numberOfTaskGenerators, new String[]{}); //?
        // TODO is the following okay?
        //createTaskGenerators(TASK_GENERATOR_CONTAINER_IMAGE, numberOfTaskGenerators, envVariablesDataGenerator);
        LOGGER.info("Task Generators created successfully.");

        // Create evaluation storage
        createEvaluationStorage();
        LOGGER.info("Evaluation Storage created successfully.");

        waitForComponentsToInitialize();
        LOGGER.info("All components initilized.");
    }

    /*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.hobbit.core.components.AbstractBenchmarkController#executeBenchmark()
     */
    @Override
    protected void executeBenchmark() throws Exception {

        // give the start signals
        sendToCmdQueue(Commands.TASK_GENERATOR_START_SIGNAL);
        sendToCmdQueue(Commands.DATA_GENERATOR_START_SIGNAL);
        LOGGER.info("Start signals sent to Data and Task Generators");

        // wait for the data generators to finish their work
        LOGGER.info("Waiting for the data generators to finish their work.");
        waitForDataGenToFinish();
        LOGGER.info("Data generators finished.");

        // wait for the task generators to finish their work
        LOGGER.info("Waiting for the task generators to finish their work.");
        waitForTaskGenToFinish();
        LOGGER.info("Task generators finished.");

        // wait for the system to terminate
        LOGGER.info("Waiting for the system to terminate.");
        waitForSystemToFinish();
        LOGGER.info("System terminated.");

        // create the evaluation module
        LOGGER.info("Will now create the evaluation module.");

        createEvaluationModule(EVALUATION_MODULE_CONTAINER_IMAGE, envVariablesEvaluationModule);
        LOGGER.info("Evaluation module was created.");

        // wait for the evaluation to finish
        LOGGER.info("Waiting for the evaluation to finish.");
        waitForEvalComponentsToFinish();
        LOGGER.info("Evaluation finished.");

        // Send the resultModule to the platform controller and terminate
        sendResultModel(this.resultModel);
        LOGGER.info("Evaluated results sent to the platform controller.");
    }
	
}

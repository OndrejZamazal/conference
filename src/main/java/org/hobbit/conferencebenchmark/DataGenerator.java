package org.hobbit.conferencebenchmark;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Map;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.hobbit.core.components.AbstractDataGenerator;
import org.hobbit.core.rabbit.RabbitMQUtils;
import org.hobbit.core.rabbit.SimpleFileSender;
import org.hobbit.conferencebenchmark.task.Task;
import org.hobbit.conferencebenchmark.util.PlatformConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.hobbit.conferencebenchmark.util.ConferenceTask;

/**
 * Re-used from the LargeBio track
 *
 * @author ernesto
 * @author ondrej
 * 
 * Modifications by ondrej on 19 Jan 2018
 * towards more tasks in one instance
 *
 */

public class DataGenerator extends AbstractDataGenerator {
	private static final Logger LOGGER = LoggerFactory.getLogger(DataGenerator.class);
	
	//protected String conference_task;
	
	protected File source_file;
	
	protected File target_file;
	
	protected File reference_file;
	
	private int taskId = 0;
	
	private String task_queue_Name;

	private Task task;
	
	//more tasks at once
	private ArrayList<Task> tasks;
	
	private SimpleFileSender sender;
	
	//specification of individual tasks
	private ArrayList<ConferenceTask> confTasks;
	
	 @Override
	 public void init() throws Exception {
	      LOGGER.info("Initializing Data Generator '" + getGeneratorId() + "'");
	      super.init();

	      //Get Env variables, no parameters now for conference ALL
	      //getEnvVariables();

	      
	      //task = new Task(Integer.toString(taskId++));
	      //more tasks at once
	      tasks = new ArrayList<Task>();
	      
	      confTasks = new ArrayList<ConferenceTask>();
	      
	      //confTasks.add(new ConferenceTask("cmt.owl","Conference.owl","reference-cmt-conference.rdf"));
/*	      confTasks.add(new ConferenceTask("cmt.owl","confOf.owl","reference-cmt-confof.rdf"));

	      confTasks.add(new ConferenceTask("cmt.owl","ekaw.owl","reference-cmt-ekaw.rdf"));	      
	      confTasks.add(new ConferenceTask("cmt.owl","edas.owl","reference-cmt-edas.rdf"));	      
	      confTasks.add(new ConferenceTask("cmt.owl","iasted.owl","reference-cmt-iasted.rdf"));
	      confTasks.add(new ConferenceTask("cmt.owl","sigkdd.owl","reference-cmt-sigkdd.rdf"));
	      
	      confTasks.add(new ConferenceTask("Conference.owl","confOf.owl","reference-conference-confof.rdf"));
	      confTasks.add(new ConferenceTask("Conference.owl","edas.owl","reference-conference-edas.rdf"));
	      confTasks.add(new ConferenceTask("Conference.owl","ekaw.owl","reference-conference-ekaw.rdf"));
	      confTasks.add(new ConferenceTask("Conference.owl","iasted.owl","reference-conference-iasted.rdf"));
	      confTasks.add(new ConferenceTask("Conference.owl","sigkdd.owl","reference-conference-sigkdd.rdf"));
*/
	      confTasks.add(new ConferenceTask("edas.owl","ekaw.owl","reference-edas-ekaw.rdf"));
/*	      confTasks.add(new ConferenceTask("confOf.owl","edas.owl","reference-confof-edas.rdf"));
	      confTasks.add(new ConferenceTask("confOf.owl","ekaw.owl","reference-confof-ekaw.rdf"));
	      confTasks.add(new ConferenceTask("confOf.owl","iasted.owl","reference-confof-iasted.rdf"));
	      confTasks.add(new ConferenceTask("confOf.owl","sigkdd.owl","reference-confof-sigkdd.rdf"));
*/	      
	      
	      confTasks.add(new ConferenceTask("cmt.owl","confOf.owl","reference-cmt-confof.rdf"));
	      confTasks.add(new ConferenceTask("Conference.owl","iasted.owl","reference-conference-iasted.rdf"));
	      
/*	      confTasks.add(new ConferenceTask("edas.owl","iasted.owl","reference-edas-iasted.rdf"));
	      confTasks.add(new ConferenceTask("edas.owl","sigkdd.owl","reference-edas-sigkdd.rdf"));
	      
	      confTasks.add(new ConferenceTask("ekaw.owl","iasted.owl","reference-ekaw-iasted.rdf"));
	      confTasks.add(new ConferenceTask("ekaw.owl","sigkdd.owl","reference-ekaw-sigkdd.rdf"));
*/	      
	      //confTasks.add(new ConferenceTask("iasted.owl","sigkdd.owl","reference-iasted-sigkdd.rdf"));

	    }

	
	
	@Override
	/**
	 * Sets the source and target ontologies, and the reference alignment
	 */
	protected void generateData() throws Exception {
		
		
		LOGGER.info("Generate data.. ");
		sender = null;
        
        try {
        	
        	//go through all datasets for tasks
            prepareTasks();
            
        } 
        catch (Exception e) {
            LOGGER.error("Exception while sending file to System Adapter or Task Generator(s).", e);
        }
		
	}
	
	
	/**
	 * 
	 */
	private void prepareTasks() throws Exception {
		
		//To be sent to the system (just dummy file)
		//prepareSource();
		prepareQueueNames();
		
		for(ConferenceTask ct : confTasks) {
			
			task = new Task(Integer.toString(taskId++));
			
			source_file = new File(PlatformConstants.datasetsPath + File.separator + ct.getOntology1());

			target_file = new File(PlatformConstants.datasetsPath + File.separator + ct.getOntology2());
	        
			reference_file = new File(PlatformConstants.datasetsPath + File.separator + ct.getReferenceAlignment());
			
			task_queue_Name = ct.getOntologyName1()+"-"+ct.getOntologyName2();
			
			LOGGER.info("Preparing task "+taskId+" in DataGenerator for the source "+source_file+" target "+target_file+" and reference "+reference_file);
	        
			//To be sent to the Task. Task will also include parameter like matching classes, properties.
			prepareReference();
			    
			//To be sent to the TaskGenerator
			prepareTask();
		}
		
		//Sends created tasks to TaskGenerator
		sendData();
		
	}
	
	/**
	 * 
	 * Sets a Task and sends it to TaskGenerator later in sendData method (target file path and relevant parameters). Dataset itself is sent via the FileSender 
	 * 
	 * @throws IOException 
	 * 
	 */
	private void prepareTask() throws IOException {
		
		LOGGER.info("Prepare target (sending source and target by sender, the queue name \"input_files\")... ");
		
		byte[][] generatedFileArray = new byte[3][];
		// send the file name and its content
		//We send both source and target paths: not necessary for largebio but useful for multfarm or conference
		
		generatedFileArray[0] = RabbitMQUtils.writeString(PlatformConstants.RDFXML);
		generatedFileArray[1] = RabbitMQUtils.writeString(source_file.getAbsolutePath());
		generatedFileArray[2] = RabbitMQUtils.writeString(target_file.getAbsolutePath());
		
		// convert them to byte[]
		byte[] generatedFile = RabbitMQUtils.writeByteArrays(generatedFileArray);
		task.setSourceTarget(generatedFile);
		LOGGER.info("Setting source and target set in preparing task (datagenerator)... ");
		
		//task.setSourceName(source_file.getName());
		//task.setTargetName(target_file.getName());
		
		//Set other parameters for the task. i.e. what to match, potentially which URLs, etc.
		//In conference we match classes and properties
		task.setMatchingClassesRequired(true);
		task.setMatchingObjectPropertiesRequired(true);
		task.setMatchingDataPropertiesRequired(true);
		task.setMatchingInstancesRequired(false);
		
		//Task name is the queue name of the ontology1-ontology2
        task.setTaskQueueName(task_queue_Name);
		
		//byte[] data = SerializationUtils.serialize(task);
		//byte[] data = SerializationUtils.serialize((Serializable) task);
		//now just store task into tasks ArrayList and in the end this will be serialized and send, see sendData method 
		tasks.add(task);
		
		LOGGER.info("Sending input_files by sender (datagenerator)... ");
		// define a queue name, e.g., read it from the environment
		String queueName = "input_files";
		
		// create the sender
		sender = SimpleFileSender.create(this.outgoingDataQueuefactory, queueName);
		
		InputStream is_source = null;
		InputStream is_target = null;
		try {
		    // create input stream, e.g., by opening a file
		    is_source = new FileInputStream(source_file);
		    is_target = new FileInputStream(target_file);
		    // send data
		    sender.streamData(is_source, source_file.getName());
		    sender.streamData(is_target, target_file.getName());
		    
		    
		} catch (Exception e) {
		    // handle exception
		} finally {
		    IOUtils.closeQuietly(is_source);
		    IOUtils.closeQuietly(is_target);
		}
		
		// close the sender
		IOUtils.closeQuietly(sender);
		
		//sendDataToTaskGenerator(data); //later see sendData() method which sends it all tasks at once
		LOGGER.info("Target data successfully sent to Task Generator.");
		
	}


	/**
	 * 
	 * We prepare the queue names where the files for the benchmark tasks will be sent
	 * This information will be treated in the method "receiveGeneratedData" of the SystemAdapter
	 * Single task benchmark will send only one queue name, while multiple task benchmarks like Conference and Multifarm will send many 
	 *  
	 * @throws IOException 
	 * 
	 */
	private void prepareQueueNames() throws IOException {
	    
		byte[][] generatedFileArray = new byte[confTasks.size()+1][];
		
		// send format of the files (inherited from hobbit tasks)
		generatedFileArray[0] = RabbitMQUtils.writeString(PlatformConstants.RDFXML);
		
		//We send queue names to system to initialize set of threads to deal with the received files from Taskgenerator
		//Send as many queues as tasks (execute din one go) in the benchmark
		int i=1;
		for(ConferenceTask ct : confTasks) {
			String task_queue_name = ct.getOntologyName1()+"-"+ct.getOntologyName2();
			generatedFileArray[i++] = RabbitMQUtils.writeString(task_queue_name);
		}
		
		// convert them to byte[]
		byte[] generatedFile = RabbitMQUtils.writeByteArrays(generatedFileArray);
		
		// send data to system
		sendDataToSystemAdapter(generatedFile);
		LOGGER.info(confTasks.size()+" queue name were sent to the system adapter.");
		
	}


	/**
	 * We sent the source dataset (path) to the System adapter. Dataset itself is sent via the FileSender 
	 * @throws IOException 
	 * deprecated
	 */
	private void prepareSource() throws IOException {

		//TODO data is sent by two means. It seems that currently only the sender is used for the dataset itself (for some size restrictions). I kept it the same as in largebio.		
		//The one that will appear as parameter e.g. byte[] data in the System Adapter or Task Generator(s) only
		//contains the format and the path to the dataset (optionally, it may also contain some relevant parameters)
		
		LOGGER.info("Prepare source (sending data to system only)... ");
		
		source_file = new File(PlatformConstants.datasetsPath + File.separator + "cmt.owl");
		
		byte[][] generatedFileArray = new byte[2][];
		
		// send the file name and its content.
		generatedFileArray[0] = RabbitMQUtils.writeString(PlatformConstants.RDFXML);
		generatedFileArray[1] = RabbitMQUtils.writeString(source_file.getAbsolutePath());
		
		// convert them to byte[]
		byte[] generatedFile = RabbitMQUtils.writeByteArrays(generatedFileArray);
		
		
		//TODO We do not need to send anything as source
		// define a queue name, e.g., read it from the environment
		/*String queueName = "source_file";
		
		// create the senderfile to System Adapter or Task Generator(s).
		sender = SimpleFileSender.create(this.outgoingDataQueuefactory, queueName);
		
		InputStream is = null;
		try {
		    // create input stream, e.g., by opening a file
		    is = new FileInputStream(source_file);
		    // send data
		    sender.streamData(is, source_file.getName());
		} catch (Exception e) {
		    // handle exception
		} finally {
		    IOUtils.closeQuietly(is);
		}
		
		// close the sender
		IOUtils.closeQuietly(sender);
		*/
		
		// send data to system
		sendDataToSystemAdapter(generatedFile);
		LOGGER.info(source_file.getAbsolutePath() + " (" + (double) source_file.length() / 1000 + " KB) sent to System Adapter.");

	}



	/**
	 * @throws IOException 
	 * 
	 */
	private void prepareReference() throws IOException {
		
		 LOGGER.info("Prepare reference alignment (setting expected answers)... ");
		
		 byte[][] generatedFileArray = new byte[3][];
		 // send the file name and its content
		 generatedFileArray[0] = RabbitMQUtils.writeString(PlatformConstants.RDFXML);
		 generatedFileArray[1] = RabbitMQUtils.writeString(reference_file.getAbsolutePath());
		 generatedFileArray[2] = FileUtils.readFileToByteArray(reference_file);
		 // convert them to byte[]
		 byte[] generatedFile = RabbitMQUtils.writeByteArrays(generatedFileArray);
		 
		 
		 task.setExpectedAnswers(generatedFile);
		
	}

	
	/**
	 * 
	 * Sends created tasks to TaskGenerator at once. Dataset itself is sent via the FileSender before.
	 * 
	 * @throws IOException 
	 * 
	 */
	private void sendData() throws IOException {
		
		LOGGER.info("Sending all created tasks to Task Generator.");
		
		byte[] data = SerializationUtils.serialize((Serializable) tasks);
		
		sendDataToTaskGenerator(data);
		LOGGER.info("Target data (created tasks) successfully sent to Task Generator.");
		
	}

}

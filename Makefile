default: build dockerize

build:
	mvn clean package -U -Dmaven.test.skip=true

dockerize:
	docker build -f conferencebenchmarkcontroller1.docker -t git.project-hobbit.eu:4567/ondrej.zamazal/conference1 .
	docker build -f conferencedatagenerator1.docker -t git.project-hobbit.eu:4567/ondrej.zamazal/conferencedatagenerator1 .
	docker build -f conferencetaskgenerator1.docker -t git.project-hobbit.eu:4567/ondrej.zamazal/conferencetaskgenerator1 .
	docker build -f conferenceevaluationmodule1.docker -t git.project-hobbit.eu:4567/ondrej.zamazal/conferenceevaluationmodule1 .

	docker push git.project-hobbit.eu:4567/ondrej.zamazal/conference1
	docker push git.project-hobbit.eu:4567/ondrej.zamazal/conferencedatagenerator1
	docker push git.project-hobbit.eu:4567/ondrej.zamazal/conferencetaskgenerator1
	docker push git.project-hobbit.eu:4567/ondrej.zamazal/conferenceevaluationmodule1
